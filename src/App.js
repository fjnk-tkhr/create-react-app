import logo from './logo.svg';
import './App.css';

function App(props) {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
          <br/><br/>会社名 ：{process.env.REACT_APP_ENV_VAR_1}
          <br/>信条 ：{process.env.REACT_APP_ENV_VAR_2}

        </a>
      </header>
    </div>
  );
}

export default App;
